object FGame: TFGame
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FGame'
  ClientHeight = 499
  ClientWidth = 1022
  Color = 9890246
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = 'Ubuntu'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Shape1: TShape
    Left = 104
    Top = 328
    Width = 65
    Height = 65
    Brush.Color = 8062173
    Pen.Color = 13360844
    Visible = False
  end
  object P1: TPanel
    Left = 0
    Top = 0
    Width = 985
    Height = 88
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 58
      Width = 113
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 208
      Top = 8
      Width = 51
      Height = 16
      Caption = 'Grid size:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 303
      Top = 8
      Width = 39
      Height = 16
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 303
      Top = 25
      Width = 39
      Height = 16
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 208
      Top = 25
      Width = 89
      Height = 16
      Caption = 'Frame duration:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 208
      Top = 42
      Width = 32
      Height = 16
      Caption = 'Walls:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 303
      Top = 42
      Width = 39
      Height = 16
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 135
      Top = 57
      Width = 121
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 208
      Top = 57
      Width = 81
      Height = 25
      Caption = 'Direction:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 295
      Top = 56
      Width = 121
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 486
      Top = 57
      Width = 121
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 422
      Top = 57
      Width = 56
      Height = 25
      Caption = 'Frame:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 701
      Top = 56
      Width = 73
      Height = 25
      Caption = 'Goodies:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 780
      Top = 56
      Width = 121
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 878
      Top = 56
      Width = 51
      Height = 25
      Caption = 'Score:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 935
      Top = 56
      Width = 121
      Height = 25
      Caption = 'Snake length:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ExitB: TButton
      Left = 8
      Top = 8
      Width = 185
      Height = 49
      Caption = 'Exit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = ExitBClick
    end
  end
  object P2: TPanel
    Left = 0
    Top = 88
    Width = 585
    Height = 201
    BevelOuter = bvNone
    TabOrder = 1
    object img: TPaintBox
      Left = 40
      Top = 32
      Width = 105
      Height = 105
      Color = clGray
      ParentColor = False
      OnPaint = imgPaint
    end
  end
  object RefreshT: TTimer
    Enabled = False
    OnTimer = RefreshTTimer
    Left = 352
    Top = 352
  end
end
