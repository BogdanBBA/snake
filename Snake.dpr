program Snake;

uses
  Vcl.Forms,
  Menu in 'Menu.pas' {FMenu},
  Game in 'Game.pas' {FGame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMenu, FMenu);
  Application.CreateForm(TFGame, FGame);
  Application.Run;
end.
