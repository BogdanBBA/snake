unit Game;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Menus, Generics.Collections;

const
  CellSize = 20;
  GoodieScorePoints = 20;

  // Directions
  DirUp = 0;
  DirRight = 1;
  DirDown = 2;
  DirLeft = 3;
  Directions: array [DirUp .. DirLeft] of string = ('Up', 'Right', 'Down', 'Left');

  // Colors
  BackgroundColor = $00DAE9E2;
  PenColor = $00CBDECC;
  SnakeColor = $000ECB7A;
  WallColor = clGray;
  GoodieColor = $007B04DD;

type
  TFGame = class(TForm)
    P1: TPanel;
    ExitB: TButton;
    P2: TPanel;
    Label1: TLabel;
    img: TPaintBox;
    Shape1: TShape;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    RefreshT: TTimer;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    procedure ExitBClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure imgPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RefreshTTimer(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    private
      { Private declarations }
    public
      { Public declarations }
  end;

type
  TGoodie = class
    public
      P: TPoint;
      LifeLeft: Word;
      constructor Create(X, Y, Lifetime: Word);
  end;

var
  FGame: TFGame;

  GridSize: TPoint;
  FrameDuration: Word;
  FrameCount, Score: LongWord;
  Direction, NewGoodieProbability: Byte;
  ChangedDirectionThisFrame: Boolean;

  Snake, Walls: TList<TPoint>;
  Goodies: TList<TGoodie>;

implementation

{$R *.dfm}


uses Menu;

constructor TGoodie.Create(X: Word; Y: Word; Lifetime: Word);
begin
  P.SetLocation(X, Y);
  LifeLeft := Lifetime
end;

function IndexOfGoodie(X, Y: Word): Integer;
var i: Word;
begin
  if Goodies.Count > 0 then
    for i := 0 to Goodies.Count - 1 do
      if (Goodies[i].P.X = X) and (Goodies[i].P.Y = Y) then
        begin
          Result := i;
          Exit
        end;
  Result := - 1
end;

procedure TFGame.ExitBClick(Sender: TObject);
begin
  if not RefreshT.Enabled then
    Self.Close
  else
    begin
      RefreshT.Enabled := False;
      if MessageDlg('Are you sure you want to quit this unfinished game?', mtConfirmation, mbYesNo, 0) = mrYes then
        Self.Close
      else
        begin
          RefreshT.Enabled := True;
          Exit
        end
    end
end;

procedure TFGame.FormResize(Sender: TObject);
begin
  P1.Width := FGame.Width;
  P2.Width := P1.Width;
  P2.Height := FGame.Height - P1.Height;
  Self.OnShow(Sender)
end;

procedure TFGame.FormShow(Sender: TObject);
  function WallPositionOK(X, Y: Word): Boolean;
  begin
    Result := True;
    if (X < 7) then
      Result := Y > 4
    else if (Y < 5) then
      Result := X > 6
  end;

var i, n, a, b: Word;
begin
  FrameDuration := FMenu.TB3.Position * 100;
  GridSize.SetLocation(FMenu.TB1.Position, FMenu.TB2.Position);
  Direction := DirRight;
  NewGoodieProbability := FMenu.TB4.Position;

  if Snake <> nil then
    FreeAndNil(Snake);
  Snake := TList<TPoint>.Create;
  if Walls <> nil then
    FreeAndNil(Walls);
  Walls := TList<TPoint>.Create;
  if Goodies <> nil then
    FreeAndNil(Goodies);
  Goodies := TList<TGoodie>.Create;

  Snake.AddRange([Point(4, 2), Point(3, 2), Point(2, 2)]);

  for i := 0 to GridSize.X - 1 do
    begin
      Walls.Add(Point(i, 0));
      Walls.Add(Point(i, GridSize.Y - 1))
    end;
  for i := 0 to GridSize.Y - 1 do
    begin
      Walls.Add(Point(0, i));
      Walls.Add(Point(GridSize.X - 1, i))
    end;

  case (FMenu.CB1.ItemIndex) of
    1:
      begin
        n := Trunc(Sqrt(GridSize.X * GridSize.Y));
        n := Round(0.75 * n + Random(Round(0.25 * n)));
        Randomize;
        for i := 1 to n do
          begin
            repeat
              a := Random(GridSize.X);
              b := Random(GridSize.Y);
            until WallPositionOK(a, b) and (Walls.IndexOf(Point(a, b)) = - 1);
            Walls.Add(Point(a, b))
          end
      end;
  end;

  img.Canvas.Brush.Color := FGame.Color;
  img.Canvas.FillRect(Rect(0, 0, img.Width, img.Height));
  img.Width := GridSize.X * CellSize + 1;
  img.Height := GridSize.Y * CellSize + 1;
  img.Left := P2.Width div 2 - img.Width div 2;
  img.Top := P2.Height div 2 - img.Height div 2;

  Label3.Caption := Format('%d x %d (%d cells)', [GridSize.X, GridSize.Y, GridSize.X * GridSize.Y]);
  Label4.Caption := Format('%d ms (%.2f frames/sec)', [FrameDuration, 1000 / FrameDuration]);
  Label7.Caption := Format('%d cells', [Walls.Count]);

  RefreshT.Enabled := False;
  FrameCount := 0;
  Score := 0;
  RefreshT.Interval := FrameDuration;
  RefreshT.Enabled := True;

  img.OnPaint(img)
end;

procedure TFGame.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(Snake);
  FreeAndNil(Walls)
end;

procedure TFGame.imgPaint(Sender: TObject);
var i, j: Word;
begin
  with img.Canvas do
    begin
      Brush.Color := BackgroundColor;
      FillRect(Rect(0, 0, img.Width, img.Height));

      Pen.Color := PenColor;
      for i := 0 to GridSize.X do
        begin
          MoveTo(i * CellSize, 0);
          LineTo(i * CellSize, img.Height)
        end;
      for j := 0 to GridSize.Y do
        begin
          MoveTo(0, j * CellSize);
          LineTo(img.Width, j * CellSize)
        end;

      Brush.Color := WallColor;
      if Walls.Count > 0 then
        for i := 0 to Walls.Count - 1 do
          FillRect(Rect(Walls[i].X * CellSize + 1, Walls[i].Y * CellSize + 1, Walls[i].X * CellSize + CellSize, Walls[i].Y * CellSize + CellSize));

      Brush.Color := GoodieColor;
      if Goodies.Count > 0 then
        for i := 0 to Goodies.Count - 1 do
          FillRect(Rect(Goodies[i].P.X * CellSize + 1, Goodies[i].P.Y * CellSize + 1, Goodies[i].P.X * CellSize + CellSize,
            Goodies[i].P.Y * CellSize + CellSize));

      Brush.Color := SnakeColor;
      if Snake.Count > 0 then
        for i := 0 to Snake.Count - 1 do
          FillRect(Rect(Snake[i].X * CellSize + 1, Snake[i].Y * CellSize + 1, Snake[i].X * CellSize + CellSize, Snake[i].Y * CellSize + CellSize));
    end
end;

procedure TFGame.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if ChangedDirectionThisFrame then
    Exit;
  case Key of
    'w', 'W':
      if Direction <> DirDown then
        Direction := DirUp;
    'd', 'D':
      if Direction <> DirLeft then
        Direction := DirRight;
    's', 'S':
      if Direction <> DirUp then
        Direction := DirDown;
    'a', 'A':
      if Direction <> DirRight then
        Direction := DirLeft;
  end;
  ChangedDirectionThisFrame := True;
end;

procedure TFGame.RefreshTTimer(Sender: TObject);
var nextCell: TPoint; i, a, b, nIter: Word;
begin
  Inc(FrameCount);
  Inc(Score);
  ChangedDirectionThisFrame := False;

  case Direction of
    DirUp:
      nextCell := Point(Snake[0].X, Snake[0].Y - 1);
    DirRight:
      nextCell := Point(Snake[0].X + 1, Snake[0].Y);
    DirDown:
      nextCell := Point(Snake[0].X, Snake[0].Y + 1);
    DirLeft:
      nextCell := Point(Snake[0].X - 1, Snake[0].Y);
  end;

  if (Walls.IndexOf(nextCell) <> - 1) or (Snake.IndexOf(nextCell) <> - 1) then
    begin
      RefreshT.Enabled := False;
      Inc(Score, - 1);
      MessageDlg(Format('Your snake died!%sYou''ve managed to get a score of %d :) !', [#13, Score]), mtInformation, [mbOk], 0);
    end
  else if IndexOfGoodie(nextCell.X, nextCell.Y) <> - 1 then
    begin
      Inc(Score, GoodieScorePoints);
      Goodies.Delete(IndexOfGoodie(nextCell.X, nextCell.Y));
      Snake.Insert(0, nextCell)
    end
  else
    begin
      Snake.Delete(Snake.Count - 1);
      Snake.Insert(0, nextCell)
    end;

  if Goodies.Count > 0 then
    for i := 0 to Goodies.Count - 1 do
      Inc(Goodies[i].LifeLeft, - 1);
  i := 0;
  while i < Goodies.Count do
    begin
      if Goodies[i].LifeLeft = 0 then
        Goodies.Delete(i);
      Inc(i)
    end;

  if Random(100) + 1 <= NewGoodieProbability then
    begin
      nIter := 0;
      repeat
        Inc(nIter);
        a := Random(GridSize.X);
        b := Random(GridSize.Y);
        if nIter = 1000 then
          Break;
      until (Walls.IndexOf(Point(a, b)) = - 1) and (Snake.IndexOf(Point(a, b)) = - 1) and (IndexOfGoodie(a, b) = - 1);
      if nIter < 1000 then
        Goodies.Add(TGoodie.Create(a, b, 11 + Random(50)))
    end;

  Label8.Caption := Format('%d', [Snake.Count]);
  Label10.Caption := Directions[Direction];
  Label11.Caption := Format('%d (%.1f sec)', [FrameCount, FrameCount / (1000 / FrameDuration)]);
  Label14.Caption := Format('%d', [Goodies.Count]);
  Label16.Caption := Format('%d', [Score]);
  img.OnPaint(img)
end;

end.
