object FMenu: TFMenu
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Snake 1.0 (March 29th, 2014)'
  ClientHeight = 416
  ClientWidth = 577
  Color = 9890246
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 78
    Height = 34
    Caption = 'Snake'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 3630157
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 39
    Width = 89
    Height = 17
    Caption = 'by BogdanBBA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 3630157
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 80
    Top = 79
    Width = 126
    Height = 17
    Caption = 'Grid width (columns):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4282960
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 80
    Top = 135
    Width = 111
    Height = 17
    Caption = 'Grid height (rows):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4282960
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 304
    Top = 79
    Width = 95
    Height = 17
    Caption = 'Frame duration:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4282960
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 192
    Top = 207
    Width = 111
    Height = 17
    Caption = 'Generated layout:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4282960
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 304
    Top = 135
    Width = 186
    Height = 17
    Caption = 'New goodie probability/frame:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4282960
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object ExitB: TButton
    Left = 184
    Top = 336
    Width = 185
    Height = 49
    Caption = 'Exit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = ExitBClick
  end
  object TB1: TTrackBar
    Left = 72
    Top = 96
    Width = 185
    Height = 33
    Max = 100
    Min = 10
    Position = 10
    TabOrder = 1
    OnChange = TB1Change
  end
  object TB2: TTrackBar
    Left = 72
    Top = 152
    Width = 185
    Height = 33
    Max = 50
    Min = 8
    Position = 8
    TabOrder = 2
    OnChange = TB2Change
  end
  object TB3: TTrackBar
    Left = 296
    Top = 96
    Width = 185
    Height = 33
    Min = 1
    Position = 10
    TabOrder = 3
    OnChange = TB3Change
  end
  object StartB: TButton
    Left = 184
    Top = 281
    Width = 185
    Height = 49
    Caption = 'Start!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = StartBClick
  end
  object CB1: TComboBox
    Left = 192
    Top = 230
    Width = 169
    Height = 25
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 5
    Text = 'None'
    Items.Strings = (
      'None'
      'A few, randomly')
  end
  object TB4: TTrackBar
    Left = 296
    Top = 152
    Width = 185
    Height = 33
    Max = 100
    Min = 1
    Position = 10
    TabOrder = 6
    OnChange = TB4Change
  end
end
