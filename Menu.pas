unit Menu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TFMenu = class(TForm)
    ExitB: TButton;
    Label1: TLabel;
    Label2: TLabel;
    TB1: TTrackBar;
    Label3: TLabel;
    TB2: TTrackBar;
    Label4: TLabel;
    TB3: TTrackBar;
    Label5: TLabel;
    StartB: TButton;
    CB1: TComboBox;
    Label6: TLabel;
    TB4: TTrackBar;
    Label7: TLabel;
    procedure ExitBClick(Sender: TObject);
    procedure TB3Change(Sender: TObject);
    procedure TB2Change(Sender: TObject);
    procedure TB1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StartBClick(Sender: TObject);
    procedure TB4Change(Sender: TObject);
    private
      { Private declarations }
    public
      { Public declarations }
  end;

var
  FMenu: TFMenu;

implementation

{$R *.dfm}


uses Game;

procedure TFMenu.FormCreate(Sender: TObject);
begin
  TB1.Position := 50;
  TB2.Position := 30;
  TB3.Position := 3;
  TB4.Position := 16
end;

procedure TFMenu.TB1Change(Sender: TObject);
begin
  Label3.Caption := Format('Grid width (columns): %d', [TB1.Position])
end;

procedure TFMenu.TB2Change(Sender: TObject);
begin
  Label4.Caption := Format('Grid height (rows): %d', [TB2.Position])
end;

procedure TFMenu.TB3Change(Sender: TObject);
begin
  Label5.Caption := Format('Frame duration: %d ms', [TB3.Position * 100])
end;

procedure TFMenu.TB4Change(Sender: TObject);
begin
  Label7.Caption := Format('New goodie probability/frame: %d%%', [TB4.Position])
end;

procedure TFMenu.ExitBClick(Sender: TObject);
begin
  FMenu.Close
end;

procedure TFMenu.StartBClick(Sender: TObject);
begin
  FGame.ShowModal
end;

end.
